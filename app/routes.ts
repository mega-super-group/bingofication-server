import {App} from "./framework/app";
import {ParseRequest} from "./middleware/parse.request";
import {BeforeRequest} from "./middleware/before.request";
import {AfterRequest} from "./middleware/after.request";
import {NotFoundController} from "./controllers/default/not.found.controller";
import {ErrorController} from "./controllers/default/error.controller";

export function routeStack() {

    /*
     *  Service hooks
     */
    App.router.handleBefore(ParseRequest.handle);
    App.router.handleBefore(BeforeRequest.handle);
    App.router.handleAfter(AfterRequest.handle);
    App.router.handleNotFound(NotFoundController.handle);
    App.router.handleError(ErrorController.handle);


}
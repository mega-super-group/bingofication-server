import {GamerUserInterface} from "./user.inteface";
import {PlayerInterface} from "./player.interface";

export interface GameInterface {
    id: number;
    createdAt: number;
    startAt: number;
    status: GameStatus;
    players: PlayerInterface[];
    maxPlayers: number;
}

export interface GameRoundInterface {
    id: number;
    playNumber: number;
}

export interface GameObjectInterface {
    addPlayer: (user: GamerUserInterface) => void;
    removePlayer: (user: GamerUserInterface) => void;
    toObject: () => GameInterface;
}

export enum GameStatus {
    PRE_INIT,
    INIT,
    OPEN,
    STARTING,
    STARTED,
    PAUSED,
    FINISHING,
    FINISHED,
    CLOSED
}

export interface GameConfigurationInterface {
    roundTime?: number;
    inRoundNotificationTime?: number;
    maxPlayers?: number;
}
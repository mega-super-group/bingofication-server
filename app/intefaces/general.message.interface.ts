export interface GeneralMessageInterface {
    id: string;
    time: number;
}

export interface MessageWithPayload<T> extends GeneralMessageInterface {
    data: T
}

export interface ErrorMessageInterface {
    code: number;
    message: string;
}

export interface MessageSerializable {
    toObject: () => {[key: string]: any};
}

export enum GeneralMessageTypes {
    GAME_LIST = "glst",
    GAME_NEW = "gnew",
    GAME_SELECT = "gsel",
    GAME_JOIN = "gjn",
    GAME_EXIT = "gexit",
    GAME_JOIN_CONFIRM = "gjnc",
    GAME_PRE_START = "gprs",
    GAME_START = "gst",
    GAME_NEW_ROUND = "gnr",
    GAME_IN_ROUND = "ginr",
    GAME_META_INF = "gmin",
    GAME_WINNER = "gwin",
    GAME_END = "gend",
    GAME_CROSS_NUMBER = "gcnm",
    GAME_VOTE_NEXT ="gnxt",
    USER_IDENTIFY = "uid",
    USER_CONFIRM_IDENTIFY = "ucid",
    USER_NEW_USER = "unew",
    USER_ACCEPTED = "uacc",
    USER_END_SESSION = "uend",
    ERROR = "err"
}
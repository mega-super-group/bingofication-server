import {TicketInterface} from "./ticket.interface";
import {UserInterface} from "./user.inteface";

export interface PlayerInterface {
    id: number;
    ai: boolean;
    ticket: TicketInterface;
    user: UserInterface;
}

export interface PlayerObjectInterface extends PlayerInterface {

    isWinner: () => boolean;
    toObject: () => PlayerInterface;

}
import {ConnectedUser} from "../models/user/connected.user";
import {Player} from "../models/player/player";

export interface UserInterface {

    uid: string;
    name: string;
    first: string;
    last: string;

}

export interface UserGuestInterface extends UserInterface {
    sid: string;
}

export interface UserCredentialsInterface extends UserInterface {
    key: string;
}

export interface SerializableUserObjectInterface extends UserInterface {

    copyFromUser: (u: UserInterface) => void;
    toObject: () => UserInterface;

}

export interface SerializableUserCredentialObjectInterface extends SerializableUserObjectInterface {

    toCredentialObject: () => UserCredentialsInterface;

}

export interface RemoteUserInterface extends SerializableUserObjectInterface {

    sid: string;
    connection: ConnectedUser;

}

export interface GamerUserInterface extends SerializableUserObjectInterface {

    players: Player[]

}


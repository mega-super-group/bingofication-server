import {PlayerInterface} from "./player.interface";
import {GameInterface, GameRoundInterface} from "./game.interface";
import {TicketNumberBlockInterface} from "./ticket.interface";

export interface GameUpdatePreStartInterface {
    players: PlayerInterface[];
    playersReady: PlayerInterface[];
    startAtTime: number;
    untilStart: number;
}

export interface GameUpdateStartInterface extends GameInterface {

}

export interface GameUpdateNewRoundInterface extends GameRoundInterface {

}

export interface GameUpdateMetaInformationInterface {
    gameId: number;
    ticketView: TicketNumberBlockInterface[][];
}

export interface GameUpdateInRoundInterface {
    playersReady: PlayerInterface[];
    untilNextRound: number;
}

export interface GameUpdateWinnerInterface {
    players: PlayerInterface[];
}

export interface GameUpdateEndInterface {
    game: GameInterface;
    playersWon: PlayerInterface[];
    endAtTime: number;
}

export interface GameCrossNumberInterface {
    number: number;
    gameId: number;
}

export interface GameVoteNextInterface {
	gameId: number;
}

export interface GamePlayerExitInterface {
    gameId: number;
	playerExit: PlayerInterface;
	players: PlayerInterface[];
}

export interface GameJoinInterface {
    id: number;
}
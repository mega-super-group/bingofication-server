export interface GameManagerStatusInterface {
	guests: number
	users: number,
	games: number,
	finishedGames: number
}
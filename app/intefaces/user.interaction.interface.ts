export interface UserIdentityMessage {
    uid: string;
    key: string;
}

export interface NewUserMessage extends UserIdentityMessage {
    name: string;
    first: string;
    last: string;
}
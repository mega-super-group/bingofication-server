export interface TicketInterface {
    id: number;
    uid: string;
    crossed: number;
    total: number;
}

export interface TicketNumberBlockInterface {
    id: number;
    matched: boolean;
}

export interface TicketNumberBlockObjectInterface extends TicketNumberBlockInterface {
    crossOut: () => void;
    toObject: () => TicketNumberBlockInterface;
}

export interface TicketObjectInterface extends TicketInterface {
    isWinner: () => boolean;
    hasNumber: (num: number) => boolean;
    toObject: () => TicketInterface;
    toView: () => TicketNumberBlockInterface[][];
}
import {MessageWithPayload} from "../intefaces/general.message.interface";
import {v1 as UUID} from "uuid";

export class MessageHelper {

    public static newMessage<T>(payload: T) : MessageWithPayload<T> {

        return {
            id: UUID(),
            time: Date.now(),
            data: payload
        }

    }

}
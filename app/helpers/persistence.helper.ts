import {UserCredentialsInterface, UserInterface} from "../intefaces/user.inteface";

export class PersistenceHelper {

    private static users: Map<string, UserCredentialsInterface> = new Map();

    public static async getUser(uid: string) : Promise<UserCredentialsInterface> {

        if(PersistenceHelper.users.has(uid))

            return PersistenceHelper.users.get(uid);

        return null;

    }

    public static async persistUser(user: UserCredentialsInterface) {

        PersistenceHelper.users.set(user.uid, user);

    }

    public static async getList() : Promise<UserCredentialsInterface[]> {

        const out = [];

        const iter =  PersistenceHelper.users.values();

        for(let u of iter)

            out.push(u);

        return out;

    }

}
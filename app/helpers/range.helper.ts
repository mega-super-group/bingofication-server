import {GameHelper} from "./game.helper";

export class RangeHelper {

    public static newNumber() : number {

        /*
         *  Calculate inclusive range
         */
        const range = GameHelper.getEndNumber() - GameHelper.getStartNumber() + 1;

        /*
         *  Return integer
         */
        return Math.floor((Math.random() * range) + GameHelper.getStartNumber());

    }

}
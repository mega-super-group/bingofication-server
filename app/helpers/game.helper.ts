import {App} from "../framework/app";

export class GameHelper {

    private static initialized = false;

    private static numberStart: number = null;

    private static numberEnd: number = null;

    public static init() {

        const gameConfig = App.config.get("game");

        if(gameConfig !== null && typeof gameConfig === "object") {

            if(gameConfig.hasOwnProperty("startNumber"))

                GameHelper.numberStart = gameConfig.startNumber;

            if(gameConfig.hasOwnProperty("endNumber"))

                GameHelper.numberEnd = gameConfig.endNumber;

        }

        if(GameHelper.numberStart === null)

            GameHelper.numberStart = 1;

        if(GameHelper.numberEnd === null)

            GameHelper.numberEnd = 100;

    }

    public static getStartNumber() : number {

        if(!GameHelper.initialized)

            GameHelper.init();

        return GameHelper.numberStart;

    }

    public static getEndNumber() : number {

        if(!GameHelper.initialized)

            GameHelper.init();

        return GameHelper.numberEnd;

    }

}
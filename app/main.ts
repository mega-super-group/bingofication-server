import {App} from "./framework/app";
import {GameInitializer} from "./models/game/game.initializer";


App.init("dist/routes").then(() => {

    /*
     *  Here is logic which should be initialized before app start
     */

    GameInitializer.init();

    /*
     *  App start
     */
    App.start();

});
import {Request, Response} from "../../framework/app";

export class NotFoundController {

    public static handle(req: Request, res: Response) : Response {

        console.log("404 NOT_FOUND " + new Date().toISOString() + " : ", req.method, req.url.toString(), req.body);
        return res.status(404).send("Not found");

    }

}
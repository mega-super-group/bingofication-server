import {TicketNumberBlock} from "./ticket.number.block";
import {RangeHelper} from "../../helpers/range.helper";
import {
	TicketInterface,
	TicketNumberBlockObjectInterface,
	TicketObjectInterface
} from "../../intefaces/ticket.interface";

export class Ticket implements TicketObjectInterface {

    private _id: number = null;

    private _uid: string = null;

    readonly view: TicketNumberBlockObjectInterface[][] = null;

    private idMap: Map<number, TicketNumberBlockObjectInterface> = new Map();

    private _crossed: number = 0;

    private _total: number = 0;
	
	/**
     * Construct ticket on base of parameters from the Game
     * --------------------------------------------------------
	 * @param id
	 * @param uid
	 * @param rows
	 * @param cols
	 */
    constructor(id: number,
                uid: string,
                rows: number,
                cols: number) {

        this.view = new Array(rows);

        this._id = id;
        this._uid = uid;
        
        for(let i = 0; i < this.view.length; i++) {
            this.view[i] = [];
        }

        this._seed(rows, cols);

    }
    
    /* --------------------------------------------------------
     *                          GETTERS
     * --------------------------------------------------------
     */
	
	/**
     * Ticket ID
     * --------------------------------------------------------
	 */
	get id(): number {

        return this._id;

    }
	
	/**
     * Ticket Uniq ID
     * --------------------------------------------------------
	 */
	get uid(): string {

        return this._uid;

    }
	
	/**
     * Amount of crossed blocks
     * --------------------------------------------------------
	 */
	get crossed(): number {

        return this._crossed;

    }
	
	/**
     * Total blocks to cross
     * --------------------------------------------------------
	 */
	get total(): number {

        return this._total;

    }
	
	/* --------------------------------------------------------
	 *                          METHODS
	 * --------------------------------------------------------
	 */
	
	/**
     * Check if ticket has some number
     * --------------------------------------------------------
	 * @param num
	 */
    public hasNumber(num: number): boolean {

        return this.idMap.has(num);

    }
	
	/**
     * Check if this ticket is a winning one
     * --------------------------------------------------------
	 */
	public isWinner(): boolean {

        return this._crossed === this._total;

    }
	
	/**
     * Cross some number out on ticket and increment crossed
     * --------------------------------------------------------
	 * @param num
	 */
	public crossNumber(num: number): void {

        if(this.idMap.has(num)) {

            const block = this.idMap.get(num);
            
            if(!block.matched) {
	            block.crossOut();
	            this._crossed++;
            }

        }

    }
	
	/**
     * Serialize this ticket to JS Object
     * --------------------------------------------------------
	 */
	public toObject() : TicketInterface {

        return {
            id: this._id,
            uid: this._uid,
            crossed: this._crossed,
            total: this._total,
        }

    }
	
	/**
     * Serialize this ticket View layer to JS Object
     * --------------------------------------------------------
	 */
	public toView() {

        const out = new Array(this.view.length);

        for(let i = 0; i < this.view.length; i++)

            out[i] = [];

        for(let i = 0; i < this.view.length; i++) {

            for(let j = 0; j < this.view[i].length; j++) {

                out[i][j] = this.view[i][j].toObject();

            }

        }

        return out;

    }
	
	/**
     * Do initial ticket seeding
     * --------------------------------------------------------
	 * @param rows
	 * @param cols
	 * @private
	 */
	private _seed(rows: number, cols: number) {

        const set: {[key: number]:boolean} = {};

        for(let i = 0; i < rows; i++) {

            for(let j = 0; j < cols; j++) {

                let num = RangeHelper.newNumber();

                while (set.hasOwnProperty(num)) {

                    num = RangeHelper.newNumber();

                }

                set[num] = true;

                const block = new TicketNumberBlock(num);

                this.view[i][j] = block;

                this.idMap.set(num, block);

                this._total++;

            }

        }

    }

}
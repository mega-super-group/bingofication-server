import {TicketNumberBlockInterface} from "../../intefaces/ticket.interface";

export class TicketNumberBlock implements TicketNumberBlockInterface {

    public id: number = null;

    public matched: boolean = false;

    constructor(id: number) {

        this.id = id;

    }

    public crossOut() {

        this.matched = true;

    }

    public toObject() {

        return {
            id: this.id,
            matched: this.matched
        }

    }

}
import {Socket} from "socket.io";

export class ConnectedUser {

    public socket: Socket = null;

    public uid: string = null;

}
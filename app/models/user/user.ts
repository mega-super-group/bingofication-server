import {ConnectedUser} from "./connected.user";
import {
	GamerUserInterface,
	RemoteUserInterface,
	UserCredentialsInterface, UserGuestInterface,
	UserInterface
} from "../../intefaces/user.inteface";
import {Player} from "../player/player";

export class User
    implements RemoteUserInterface, GamerUserInterface,
        UserCredentialsInterface, UserGuestInterface {

    private _sid: string = null;

    private _uid: string = null;

    private _key: string = null;

    private _name: string = null;

    private _first: string = null;

    private _last: string = null;

    private _connection: ConnectedUser = null;

    private _players: Player[];

    constructor(user?: UserInterface & UserCredentialsInterface) {

        this.copyFromUser(user);

    }

    get sid(): string {
        return this._sid;
    }

    get uid(): string {
        return this._uid;
    }

    get key(): string {
        return this._key;
    }

    set key(value: string) {
        this._key = value;
    }

    get name(): string {
        return this._name;
    }

    set name(value: string) {
        this._name = value;
    }

    get first(): string {
        return this._first;
    }

    set first(value: string) {
        this._first = value;
    }

    get last(): string {
        return this._last;
    }

    set last(value: string) {
        this._last = value;
    }

    get connection(): ConnectedUser {
        return this._connection;
    }

    set connection(value: ConnectedUser) {
        this._connection = value;
    }

    get players(): Player[] {
        return this._players;
    }

    public assignPlayer(player: Player) {

        this._players.push(player);
        player.user = this;

    }

    public revokePlayer(player: Player) {

        if(player.user.uid === this.uid) {

            this._players = this._players.filter((plr) => {

                return player.id === plr.id;

            });

            player.user = null;

        }

    }

    public setSIDOnce(sid: string) {

        if(this._sid === null)
            this._sid = sid;

    }

    public setUIDOnce(uid: string) {

        if(this._uid === null)
            this._uid = uid;

    }

    public toObject() {

        return {
            uid: this._uid,
            first: this.first,
            last: this.last,
            name: this.name
        }

    }

    public toCredentialObject() {
        return {
            uid: this._uid,
            key: this._key,
            first: this._first,
            last: this._last,
            name: this._name
        }
    }

    public copyFromUser(user: UserInterface & UserCredentialsInterface) {

        if(typeof user !== "object")

            return;
	
        const abs = user as any;
        
	    if(abs.hasOwnProperty("sid"))
		    this._sid = abs.sid;
        if(user.hasOwnProperty("uid"))
            this._uid = user.uid;
        if(user.hasOwnProperty("uid"))
            this._key = user.key;
        if(user.hasOwnProperty("name"))
            this._name = user.name;
        if(user.hasOwnProperty("first"))
            this._first = user.first;
        if(user.hasOwnProperty("last"))
            this._last = user.last;

    }

}
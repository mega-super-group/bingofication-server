import {RangeHelper} from "../../helpers/range.helper";
import {Game} from "./game";
import {GameRound} from "./engine/game.round";
import {PlayerObjectInterface} from "../../intefaces/player.interface";
import {clearInterval} from "timers";
import {Player} from "../player/player";
import {GeneralMessageTypes} from "../../intefaces/general.message.interface";
import {MessageHelper} from "../../helpers/message.helper";
import {
	GamePlayerExitInterface,
	GameUpdateEndInterface,
	GameUpdateInRoundInterface, GameUpdateMetaInformationInterface,
	GameUpdateNewRoundInterface,
	GameUpdatePreStartInterface,
	GameUpdateStartInterface, GameUpdateWinnerInterface
} from "../../intefaces/game.update.message.interface";
import {App} from "../../framework/app";
import {GameInterface} from "../../intefaces/game.interface";

export class GameEngine {

	public running = true;
	
    public game: Game = null;

    public rounds: GameRound[] = [];

    public numberSet: {[key: number]: boolean} = {};

    public numberOrder: number[] = [];

    public playersReady: PlayerObjectInterface[] = [];

    public untilNextRound = 0;
    
    constructor(game: Game) {

        this.game = game;

    }
	
	/**
	 * Check that every player is awaiting (ready) for next round to start
     * --------------------------------------------------------------------------
	 */
	public readyForNextRound(): boolean {

        const players = this.game.players;

        const realPlayers = players.filter(p => !p.ai);

        let matched = 0;

        this.playersReady.filter(p => {

            const found = realPlayers.filter(rp => p.id === rp.id);

            if(found.length > 0)

                matched++;

        });
        
        return matched >= realPlayers.length;

    }
	
	/**
	 * Start next round and all related events
     * --------------------------------------------------------------------------
	 */
	public nextRound(): number {

        let num = RangeHelper.newNumber();

        while(this.numberSet.hasOwnProperty(num)) {

            num = RangeHelper.newNumber();

        }

        this.numberSet[num] = true;

        this.numberOrder.push(num);

        this.startRound(num);

        return num;

    }
	
	/**
	 * Do the updates for current game round (player updates)
     * --------------------------------------------------------------------------
	 * @param forceful
	 */
	public propagateRoundUpdates(forceful = false) : void {

        const players = this.game.players;

        for(const p of players) {

            if(p.ai || forceful) {

                p.updateTicket(this.rounds[this.rounds.length - 1].playNumber);

                this.setPlayerAsReady(p);

            }

        }

    }
	
	/**
	 * Put some player in the ready for next round players list
     * --------------------------------------------------------------------------
	 * @param p
	 */
	public setPlayerAsReady(p: Player) {

        const res = this.playersReady.filter(pl => pl.id === p.id);
        
        if(res.length === 0) {
	
	        this.playersReady.push(p);
	
	        this.notifyInRound(this.untilNextRound);
	        
        }

    }
	
	/**
	 * Exclude player from current game
	 * --------------------------------------------------------------------------
	 * @param p
	 */
	public setPlayerAsSurrendered(p: Player): boolean {
		
		const res = this.playersReady.filter(pl => pl.id === p.id);
		
		if(res.length === 1) {
			
			this.playersReady = this.playersReady.filter(pl => pl.id !== p.id);
			
		}
		
		if(this.game.__removePlayer(p)) {
			
			this.notifyPlayerExit(p);
			
			return true;
			
		}
		
		return false;
		
    }
    
	/**
	 * Check for winning situation among players
     * --------------------------------------------------------------------------
	 */
	public checkWinner() : PlayerObjectInterface[] {

        const out = [];

        const players = this.game.players;

        for(const p of players) {

            if(p.isWinner())

                out.push(p);

        }

        return out;

    }
	
	/**
	 * Start new round counter and propagate related events
     * --------------------------------------------------------------------------
	 * @param num
	 */
	public startRound(num: number) {

        const round = new GameRound(this.rounds.length + 1, num);

        this.rounds.push(round);

        const startedAt = Date.now();

        const timeLimitAt = startedAt + (this.game.roundTimeLength * 1000);

        let timeSpent = 0;

        let timeInterval = 100;

        this.dropPlayersReady();

        this.notifyRound(round);

        this.propagateRoundUpdates();

        this.notifyTicketView();

        const iv = setInterval(() => {

            const winners = this.checkWinner();

            if(!this.running) {
	            clearInterval(iv);
	            this.dropPlayersReady();
	            this.game.finishGame();
	            this.notifyGameEnd(winners, Date.now());
	            return;
            }
            
            if(this.game.players.length === 0) {
		
		        clearInterval(iv);
	
	            this.game.finishGame();
	
	            this.dropPlayersReady();
	            
		        this.notifyGameEnd(winners, Date.now());
		
		        return;
		
	        }
            else if(winners.length > 0) {

                clearInterval(iv);

                this.notifyWinner(winners);

                this.game.finishGame();

                this.dropPlayersReady();

                this.notifyGameEnd(winners, Date.now());

                return;

            } else if(this.readyForNextRound()) {

                clearInterval(iv);
	
	            this.propagateRoundUpdates(true);
	            
                this.nextRound();

                return;

            }

            /*
             *  Notify every second about current situation
             */
            timeSpent += timeInterval;

            this.untilNextRound = Math.floor((timeLimitAt - Date.now()) / 1000);
            
            if(timeSpent % this.game.inRoundNotificationsEveryMs === 0)

                this.notifyInRound(this.untilNextRound);

            /*
             *  Propagate round updates to check off
             *  that every player is ready to continue
             */
            if(timeLimitAt < Date.now()) {

                this.propagateRoundUpdates(true);

            }

        }, timeInterval);

    }
	
	/**
	 * Start the events while awaiting game to start
     * --------------------------------------------------------------------------
	 */
	public waitingForGameToStart() {

        const iv = setInterval(() => {

            const secondsToStart = this.game.startAt - Date.now();
	
	        if(!this.running) {
		        clearInterval(iv);
		        this.dropPlayersReady();
		        this.game.finishGame();
		        return;
	        }
	        
            if(this.game.startAt <= Date.now() || this.readyForNextRound()) {

                clearInterval(iv);

                let activePlayers = this.game.players;
                
                let activeCount = 0;

                for(const p of activePlayers) {

                    if(!p.ai)

                        activeCount++;

                }

                if(activeCount > 0) {

                    this.dropPlayersReady();

                    this.notifyGameStart();

                    this.nextRound();

                    App.success("Game "
                        + this.game.id + " started");

                    return;

                }

                App.log("Game "
                    + this.game.id +
                    " wasn't able to be started because no players were" +
                    " accept start of the game");

                this.game.finishGame();

            }

            this.notifyGameAwait(secondsToStart);

        }, 1000);

    }
	
	/**
	 * Notify when new Round is happened
     * --------------------------------------------------------------------------
	 * @param round
	 */
	public notifyRound(round: GameRound) {

        for(const p of this.game.players) {

            if(!p.ai) {

                p.user
                    .connection
                    .socket
                    .emit(
                        GeneralMessageTypes.GAME_NEW_ROUND,
                        MessageHelper.newMessage<GameUpdateNewRoundInterface>(
                            round.toObject()
                        )
                    );

            }

        }

    }
	
	/**
	 * Broadcast ticket view inside the Game Engine per player
     * --------------------------------------------------------------------------
	 */
	public notifyTicketView() {

        for(const p of this.game.players) {

            if(!p.ai) {

                p.user
                    .connection
                    .socket
                    .emit(
                        GeneralMessageTypes.GAME_META_INF,
                        MessageHelper.newMessage<GameUpdateMetaInformationInterface>({
                            gameId: this.game.id,
                            ticketView: p.ticket.toView()
                        })
                    );

            }

        }

    }
	
	/**
	 * Broadcast situation happening while round is in play
     * --------------------------------------------------------------------------
	 * @param seconds
	 */
	public notifyInRound(seconds: number) {

        for(const p of this.game.players) {

            if(!p.ai) {

                p.user
                    .connection
                    .socket
                    .emit(
                        GeneralMessageTypes.GAME_IN_ROUND,
                        MessageHelper.newMessage<GameUpdateInRoundInterface>({
                            playersReady: this.playersReady.map(p => p.toObject()),
                            untilNextRound: seconds
                        })
                    );

            }

        }

    }
	
	/**
	 * Notify that Game have a winner
     * --------------------------------------------------------------------------
	 * @param winner
	 */
	public notifyWinner(winner: PlayerObjectInterface[]) {

        for(const p of this.game.players) {

            if(!p.ai) {

                p.user
                    .connection
                    .socket
                    .emit(
                        GeneralMessageTypes.GAME_WINNER,
                        MessageHelper.newMessage<GameUpdateWinnerInterface>({
                            players: winner.map(w => w.toObject())
                        })
                    );

            }

        }

    }
	
	/**
	 * Notify while game is starting
     * --------------------------------------------------------------------------
	 * @param seconds
	 */
	public notifyGameAwait(seconds: number) {

        seconds = seconds < 0 ? 0 : seconds;

        for(const p of this.game.players) {

            if(!p.ai) {

                p.user
                    .connection
                    .socket
                    .emit(
                        GeneralMessageTypes.GAME_PRE_START,
                        MessageHelper.newMessage<GameUpdatePreStartInterface>({
                            players: this.game.players.map(p => p.toObject()),
                            playersReady: this.playersReady.map(p => p.toObject()),
                            startAtTime: this.game.startAt,
                            untilStart: seconds
                        })
                    );

            }

        }

    }
	
	/**
	 * Notify Game Start event
     * --------------------------------------------------------------------------
	 */
	public notifyGameStart() {

        for(const p of this.game.players) {

            if(!p.ai) {

                p.user
                    .connection
                    .socket
                    .emit(
                        GeneralMessageTypes.GAME_START,
                        MessageHelper.newMessage<GameUpdateStartInterface>(
                            this.game.toObject()
                        )
                    );

            }

        }

    }
	
	/**
	 * Notify Game End event
     * --------------------------------------------------------------------------
	 * @param winner
	 * @param time
	 */
	public notifyGameEnd(winner: PlayerObjectInterface[], time: number) {

        for(const p of this.game.players) {

            if(!p.ai) {

                p.user
                    .connection
                    .socket
                    .emit(
                        GeneralMessageTypes.GAME_END,
                        MessageHelper.newMessage<GameUpdateEndInterface>({
                            game: this.game.toObject(),
                            playersWon: winner.map(w => w.toObject()),
                            endAtTime: time
                        })
                    );

            }

        }

    }
	
	/**
	 * Broadcast player Join game event
     * --------------------------------------------------------------------------
	 * @param player
	 */
	public notifyGameJoin(player: PlayerObjectInterface) {
	
	    for(const p of this.game.players) {
		
		    if(!p.ai) {
				
			    p.user
				    .connection
				    .socket
				    .emit(
					    GeneralMessageTypes.GAME_JOIN,
					    MessageHelper.newMessage<GameInterface>(this.game.toObject())
				    );
			
		    }
		
	    }
     
    }
	
	/**
	 * Broadcast player exit event
	 * --------------------------------------------------------------------------
	 * @param player
	 */
	public notifyPlayerExit(player: PlayerObjectInterface) {
	
		this.notifyPlayerBeforeExit(player);
		
	    for(const p of this.game.players) {
		
		    if (!p.ai) {
		
			    p.user
				    .connection
				    .socket
				    .emit(
					    GeneralMessageTypes.GAME_EXIT,
					    MessageHelper.newMessage<GamePlayerExitInterface>({
						    gameId: this.game.id,
						    playerExit: player.toObject(),
						    players: this.game.players.map(plr => plr.toObject())
					    })
				    );
			
		    }
	    }
    }
	
	/**
	 * Notify single player before exit
	 * --------------------------------------------------------------------------
	 * @param player
	 */
	public notifyPlayerBeforeExit(player: PlayerObjectInterface) {
	
	    const plr = player as Player;
	    
	    plr.user.connection.socket.emit(
		    GeneralMessageTypes.GAME_EXIT,
		    MessageHelper.newMessage<GamePlayerExitInterface>({
			    gameId: this.game.id,
			    playerExit: player.toObject(),
			    players: this.game.players.map(plr => plr.toObject())
		    })
	    );
		
    }
	
	/**
     * --------------------------------------------------------------------------
	 */
	public dropPlayersReady() {

        this.playersReady = [];

    }

}
import {App} from "../../framework/app";
import {ConnectedUser} from "../user/connected.user";
import {GameManager} from "./game.manager";
import {User} from "../user/user";
import {
	RemoteUserInterface,
	UserCredentialsInterface, UserGuestInterface,
} from "../../intefaces/user.inteface";
import {NewUserMessage, UserIdentityMessage} from "../../intefaces/user.interaction.interface";
import {
    GeneralMessageTypes,
    MessageWithPayload
} from "../../intefaces/general.message.interface";
import {v4 as UUID} from "uuid";
import {MessageHelper} from "../../helpers/message.helper";
import {PersistenceHelper} from "../../helpers/persistence.helper";
import {GameInterface} from "../../intefaces/game.interface";
import {
	GameCrossNumberInterface,
	GameJoinInterface, GamePlayerExitInterface,
	GameVoteNextInterface
} from "../../intefaces/game.update.message.interface";

export class GameInitializer {

    /**
     *  Initialize Game entrance behavior
     *  --------------------------------------------------------------------------
     */
    public static init() {

        const ws = App.webSocketHandler;

        ws.on("connection", (client => {

            const remoteUser = new ConnectedUser();

            remoteUser.socket = client;

            const user = new User();

            user.connection = remoteUser;
            user.setSIDOnce(UUID());

            App.log("Client connected","connection::client");

            GameManager.addGuest(user);

            GameInitializer.handleUserPreAuthorized(user);

            client.on("disconnect", () => {
            
                GameManager.disconnectUser(user);
            
            });
            
        }));
        
        const iv = setInterval(() => {
        	
        	App.log(GameManager.statusString());
        	
        }, 5000);

    }

    /**
     * Handle User actions on pre-active state
     * --------------------------------------------------------------------------
     * @param user
     */
    private static handleUserPreAuthorized(user: RemoteUserInterface) {

        const conn = user.connection.socket;

        if(!conn.disconnected) {

            /*
             *  RCV REQUEST
             *
             *  User identity resolution
             */
            conn.on(
                GeneralMessageTypes.USER_IDENTIFY,
                async (msg: MessageWithPayload<UserIdentityMessage>) => {

                if(msg.hasOwnProperty("data")) {

                    const u = await PersistenceHelper.getUser(msg.data.uid);

                    if(u !== null) {

                        if(u.key === msg.data.key) {

                            user.copyFromUser(u);

                            GameInitializer.promoteUser(user);

                            return;

                        }

                    }

                }

                /*
                 *  ASK REQUEST
                 *
                 *  User identity resolution
                 */
                const nu: NewUserMessage = {
                    name: null,
                    first: null,
                    last: null,
                    uid: null,
                    key: null
                };

                conn.emit(
                    GeneralMessageTypes.USER_NEW_USER,
                    MessageHelper.newMessage<NewUserMessage>(nu)
                );

            });

            /*
             *  RCV REQUEST
             *
             *  New user register
             */
            conn.on(
                GeneralMessageTypes.USER_NEW_USER,
                async (msg: MessageWithPayload<NewUserMessage>) => {

                    if(msg.hasOwnProperty("data")) {

                    	const UID = UUID();
                    	
                        const copyUser: UserCredentialsInterface & UserGuestInterface = {
                        	sid: user.sid,
                            uid: UUID(),
                            key: UUID(),
                            name: msg.data.name,
                            first: msg.data.first,
                            last: msg.data.last
                        };
	
	                    user.copyFromUser(copyUser);
	                    
	                    const u = user as User;

                        await PersistenceHelper.persistUser(u);

                        GameInitializer.promoteUser(u);

                        conn.emit(
                            GeneralMessageTypes.USER_ACCEPTED,
                            MessageHelper.newMessage<NewUserMessage>(u.toCredentialObject())
                        );

                        return;

                    }

                    // Disconnect if message wasn't good
                    conn.disconnect(true);

            });

            /*
             *  RCV REQUEST
             *
             *  User breaking session
             */
            conn.on(GeneralMessageTypes.USER_END_SESSION,
                () => {
	
	                GameManager.disconnectUser(user);
	                
                    // Disconnect request
                    conn.disconnect(true);

            });

            /*
             *  ASK REQUEST
             *
             *  User identity resolution start
             */
            conn.emit(GeneralMessageTypes.USER_CONFIRM_IDENTIFY, {});

        }

    }

    /**
     * Handle User actions when he comes to authorized state
     * --------------------------------------------------------------------------
     * @param user
     */
    public static handleUserAuthorized(user: RemoteUserInterface) {

        const conn = user.connection.socket;

        if(!conn.disconnected) {

            conn.on(
                GeneralMessageTypes.GAME_LIST,
                async () => {

                    const list = GameManager.getGames();

                    const out = [];

                    for(const g of list)

                        out.push(g.toObject());

                    conn.emit(GeneralMessageTypes.GAME_LIST,
                        MessageHelper.newMessage<GameInterface[]>(out));

                });

            /*
             *  Listen for requests to Join some game
             */
            conn.on(
                GeneralMessageTypes.GAME_JOIN,
                async (msg: MessageWithPayload<GameJoinInterface>) => {

                    if (msg.hasOwnProperty("data")) {

                        const game = GameManager.getGame(msg.data.id);

                        console.log("JOINING GAME", game, msg);

                        if (game !== null) {

                            const success = game.addPlayer(user as User);

                            // Provide response on success of joining
                            if(success) {
                             
	                            conn.emit(GeneralMessageTypes.GAME_JOIN_CONFIRM,
		                            MessageHelper
			                            .newMessage<GameInterface>(game.toObject())
	                            );
	                            
                            }

                        }

                    }
                });
	
	        /*
             *  Listen for requests which asking for new Game to be instantiated
	         */
	        conn.on(
                GeneralMessageTypes.GAME_NEW,
                async () => {

                    const game = GameManager.createGame(user as User);

                    conn.emit(GeneralMessageTypes.GAME_NEW,
                        MessageHelper.newMessage<GameInterface>(game.toObject()));

                    game.startGame();

                });
	
	        /*
             *  Listen for requests to provide details about some game selected
	         */
	        conn.on(
                GeneralMessageTypes.GAME_SELECT,
                async () => {



                });
            
            /*
             *  Listen for player requests for cross a number in tiicket
             */
            conn.on(GeneralMessageTypes.GAME_CROSS_NUMBER,
                async (msg: MessageWithPayload<GameCrossNumberInterface>) => {
                
                    const game = GameManager.getGame(Number(msg.data.gameId));
                    
                    if(game !== null) {
                        
                        if(game.hasUserAsPlayer(user)) {
                            
                            const p = game.getPlayerForUser(user);
                            
                            if(p.updateTicket(msg.data.number)) {
                             
	                            game.setPlayerIdAsReady(p);
	
	                            game.broadcastGameMetadata();
                             
                            }
                            
                        }
                        
                    }
                
            });
	
	        /*
			 *  Listen for player requests for cross a number in tiicket
			 */
	        conn.on(GeneralMessageTypes.GAME_VOTE_NEXT,
		        async (msg: MessageWithPayload<GameVoteNextInterface>) => {
			
			        const game = GameManager.getGame(Number(msg.data.gameId));
			
			        if(game !== null) {
				
				        if(game.hasUserAsPlayer(user)) {
					
					        const p = game.getPlayerForUser(user);
					
					        game.setPlayerIdAsReady(p);
					
				        }
				
			        }
			
		        });
	
	        /*
			 *  Listen for player requests for cross a number in tiicket
			 */
	        conn.on(GeneralMessageTypes.GAME_EXIT,
		        async (msg: MessageWithPayload<GamePlayerExitInterface>) => {
			
			        const game = GameManager.getGame(Number(msg.data.gameId));
			
			        if(game !== null) {
				
				        if(game.hasUserAsPlayer(user)) {
				        	
					        game.removePlayer(user as User);
					        
				        }
				
			        }
			
		        });

        }

    }

    /**
     * Promote User next
     * --------------------------------------------------------------------------
     * @param user
     */
    private static promoteUser(user: RemoteUserInterface) {

        GameManager.promoteGuest(user.sid);

        GameInitializer.handleUserAuthorized(user);

    }

}
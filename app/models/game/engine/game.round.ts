import {Player} from "../../player/player";

export class GameRound {

    public id: number = null;

    public playNumber: number = null;

    constructor(id: number, num: number) {

        this.id = id;

        this.playNumber = num;

    }

    public toObject() {

        return {
            id: this.id,
            playNumber: this.playNumber
        }

    }

}
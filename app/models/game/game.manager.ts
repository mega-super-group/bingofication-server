import {Game} from "./game";
import {GamerUserInterface, RemoteUserInterface} from "../../intefaces/user.inteface";
import {App} from "../../framework/app";
import {User} from "../user/user";
import {GameStatus} from "../../intefaces/game.interface";
import {GameManagerStatusInterface} from "../../intefaces/game.manager.interface";

export class GameManager {

    private static guests: Map<string, RemoteUserInterface> = new Map();

    private static users: Map<string, RemoteUserInterface> = new Map();

    private static games: Map<number, Game> = new Map();

    private static finishedGames: Game[] = [];

    private static gameCounter: number = 0;
	
	/**
     * New Game will be created with User as first user
     * --------------------------------------------------------------------------
	 * @param user
	 */
	public static createGame(user: GamerUserInterface & RemoteUserInterface) : Game {

        const game = new Game(++this.gameCounter);

        game.addPlayer(user);

        GameManager.games.set(game.id, game);

        return game;

    }
	
	/**
     * Get game for some id
     * --------------------------------------------------------------------------
	 * @param id
	 */
	public static getGame(id: number) : Game {

        if(GameManager.games.has(id))

            return GameManager.games.get(id);

        return null;

    }
	
	/**
     * Get all games which are in state of pre-start
     * --------------------------------------------------------------------------
	 */
	public static getGames() : Game[] {

        const out = [];

        const iterable = this.games.values();

        // Accumulate all games which isn't yet started
        for(const game of iterable) {

            if(Date.now() < game.startAt)
                
                out.push(game);

        }

        return out.sort((a, b) => {

            return a.createdAt - b.createdAt;

        });

    }
	
	/**
     * Close some game and archive it
     * --------------------------------------------------------------------------
	 * @param id
	 */
	public static closeGame(id: number) : Game {

        if(GameManager.games.has(id)) {

            const game = GameManager.games.get(id);

            game.status = GameStatus.FINISHED;

            GameManager.games.delete(id);

            GameManager.finishedGames.push(game);

            return game;

        }

        return null;

    }
	
	/**
     * Add guest User (unauthorized)
     * --------------------------------------------------------------------------
	 * @param user
	 */
	public static addGuest(user: RemoteUserInterface) {

        GameManager.guests.set(user.sid, user);

    }
	
	/**
     * Disconnect user from the memory
     * --------------------------------------------------------------------------
	 * @param user
	 */
	public static disconnectUser(user: RemoteUserInterface) {
    
        if(GameManager.guests.has(user.sid)) {
            GameManager.guests.delete(user.sid);
        }
        
        if(GameManager.users.has(user.uid)) {
            
            const gameUser = GameManager.users.get(user.uid);
            const games = GameManager.getGames();
            
            for(const game of games) {
                
                if(game.hasUserAsPlayer(gameUser)) {
                    
                    game.removePlayer(gameUser as User);
                    
                }
                
            }
            
        }
    
    }
	
	/**
     * Get server status of connected clients and games
     * --------------------------------------------------------------------------
	 */
	public static status(): GameManagerStatusInterface {
        
        return {
            guests: GameManager.guests.size,
            users: GameManager.users.size,
            games: GameManager.games.size,
            finishedGames: GameManager.finishedGames.length
        }
        
    }
	
	/**
     * Get status string
     * --------------------------------------------------------------------------
	 */
	public static statusString(): string {
	    
	    const s = GameManager.status();
	    
	    return "Server status: Guests: " + s.guests +
            " Users: " + s.users +
            " Games: " + s.games +
            " Finished games: " + s.finishedGames;
	    
    }
	
	/**
     * Promote some guest to User
     * --------------------------------------------------------------------------
	 * @param sid
	 */
	public static promoteGuest(sid: string) {

        if(!GameManager.guests.has(sid)) {

            return;

        }

        const user = GameManager.guests.get(sid);
        
        GameManager.users.set(user.uid, user);
        GameManager.guests.delete(user.sid);

        App.log("User " + user.uid + " promoted to active player", "connection::client::active");

    }

}
import {Player} from "../player/player";
import {GamerUserInterface, RemoteUserInterface, UserInterface} from "../../intefaces/user.inteface";
import {
	GameConfigurationInterface,
	GameInterface,
	GameObjectInterface,
	GameStatus
} from "../../intefaces/game.interface";
import {User} from "../user/user";
import {GameEngine} from "./game.engine";
import {GameManager} from "./game.manager";
import {App} from "../../framework/app";

export class Game implements GameObjectInterface {

    private _id: number = null;

    private _engine: GameEngine = null;

    private playerIdCounter = 0;

    private ticketIdCounter = 0;

    readonly _createdAt: number = 0;

    private _startAt: number = 0;

    private _status: GameStatus = GameStatus.PRE_INIT;

    private _players: Player[] = [];

    private playersMap: Map<number, Player> = new Map();

    readonly _maxPlayers: number = 32;

    private _ticketRows: number = 5;

    private _ticketCols: number = 5;
    
    readonly _roundTimeLength: number = 20;
    
    readonly _inRoundNotificationsEveryMs: number = 1000;

    constructor(id: number, config?: GameConfigurationInterface) {

        this._id = id;
        this._createdAt = Date.now();
        this._startAt = this._createdAt + (60 * 1000);
        this._engine = new GameEngine(this);
        
        if(config) {
            
            if(typeof config.maxPlayers !== "undefined") {
                this._maxPlayers = config.maxPlayers;
            }
	
	        if(typeof config.inRoundNotificationTime !== "undefined") {
		        this._inRoundNotificationsEveryMs = config.inRoundNotificationTime;
	        }
	
	        if(typeof config.roundTime !== "undefined") {
		        this._roundTimeLength = config.roundTime;
	        }
         
        }

    }

    get id(): number {
        return this._id;
    }
    
    get createdAt(): number {
        return this._createdAt;
    }

    get startAt(): number {
        return this._startAt;
    }

    set startAt(value: number) {
        this._startAt = value;
    }

    get status(): GameStatus {
        return this._status;
    }

    set status(value: GameStatus) {
        this._status = value;
    }

    get players(): Player[] {
        return this._players;
    }

    get maxPlayers(): number {
        return this._maxPlayers;
    }

    get ticketRows(): number {
        return this._ticketRows;
    }

    get ticketCols(): number {
        return this._ticketCols;
    }
    
	get roundTimeLength(): number {
		return this._roundTimeLength;
	}
	
	get inRoundNotificationsEveryMs(): number {
		return this._inRoundNotificationsEveryMs;
	}
	
	public addPlayer(user: GamerUserInterface & RemoteUserInterface): boolean {

        // Disallow game Join after game already started
        if(Date.now() > this.startAt)
            return false;
        
        const p = new Player(this);
        p.setUserOnce(user as User);

        for(let i = 0; i < this._players.length; i++) {

            const cp = this._players[i];

            if(cp.user.uid === user.uid)

                return false;

        }

        this._players.push(p);
        this.playersMap.set(p.id, p);
        this._engine.notifyGameJoin(p);

        App.success("Player " + p.id + " added to the game " + this.id);

        return true;
        
    }

    public removePlayer(user: GamerUserInterface & RemoteUserInterface) {
	
        let player = null;
        
        // Seek for player based on User ID
	    for(let i = 0; i < this._players.length; i++) {
		
		    const cp = this._players[i];
		
		    if(cp.user.uid === user.uid) {
			
			    player = cp;
			    break;
		    }
		
	    }
	    
	    if(player !== null) {
	       
	        return this._engine.setPlayerAsSurrendered(player);
	       
        }
        
        return false;
	    
     
    }
    
    public hasUserAsPlayer(user: UserInterface) : boolean {
        
        for(const p of this.players) {
            
            if(p.user.uid === user.uid)
                
                return true;
            
        }
        
        return false;
        
    }
    
    public getPlayerForUser(user: UserInterface) : Player {
	
	    for(const p of this.players) {
		
		    if(p.user.uid === user.uid)
			
			    return p;
		
	    }
	
	    return null;
     
    }
    
    public setPlayerIdAsReady(p: Player) {
        
        if(this.playersMap.has(p.id)) {
            
            this._engine.setPlayerAsReady(this.playersMap.get(p.id));
            
        }
        
    }
    
    public broadcastGameMetadata() {
        
        this._engine.notifyTicketView();
        
    }
    
    public getWinningNumbers() {
        
        return this._engine.numberOrder;
        
    }

    public __nextPLayerId() {

        return ++this.playerIdCounter;

    }

    public __nextTicketId() {

        return ++this.ticketIdCounter;

    }

    public __removePlayer(player: Player): boolean {
        
        if(this.playersMap.has(player.id)) {
            
            this.playersMap.delete(player.id);
            
            this._players = this._players.filter(pl => pl.id !== player.id);
            
            return true;
            
        }
        
        return false;

    }

    public startGame() {

        this._engine.waitingForGameToStart();

    }

    public finishGame() {

        GameManager.closeGame(this.id);
        
        this._engine.running = false;

    }

    public toObject() : GameInterface {

        return {
            id: this._id,
            createdAt: this._createdAt,
            startAt: this._startAt,
            status: this._status,
            players: this._players.map(p => p.toObject()),
            maxPlayers: this._maxPlayers
        }

    }

}

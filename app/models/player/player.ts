import {Ticket} from "../entities/ticket";
import {User} from "../user/user";
import {Game} from "../game/game";
import {PlayerObjectInterface} from "../../intefaces/player.interface";
import {v4 as UUID} from "uuid";
import {GamerUserInterface, RemoteUserInterface} from "../../intefaces/user.inteface";

export class Player implements PlayerObjectInterface {

    private _id: number = null;

    private _ai: boolean = false;

    private _user: GamerUserInterface & RemoteUserInterface = null;

    private _game: Game = null;

    private _ticket: Ticket = null;

    constructor(game: Game)
    {

        // Create player with ID from GAME
        this._id = game.__nextPLayerId();

        // Create UNIQ ticket for player based on ID provided by GAME
        this._ticket = new Ticket(
            game.__nextTicketId(),
            UUID(),
            game.ticketRows,
            game.ticketCols);

        // Assign game ref
        this._game = game;

    }

    get id(): number {
        return this._id;
    }

    set id(value: number) {
        this._id = value;
    }

    get ticket(): Ticket {
        return this._ticket;
    }

    set ticket(value: Ticket) {
        this._ticket = value;
    }

    get game(): Game {
        return this._game;
    }

    set game(value: Game) {
        this._game = value;
    }

    get user(): GamerUserInterface & RemoteUserInterface {
        return this._user;
    }

    set user(value: GamerUserInterface & RemoteUserInterface) {
        this._user = value;
    }

    get ai(): boolean {
        return this._ai;
    }

    set ai(value: boolean) {
        this._ai = value;
    }

    public isWinner() {

        return this.ticket.isWinner();

    }

    public updateTicket(num: number) : boolean {
        
        const nums = this._game.getWinningNumbers();
        
        if(nums.indexOf(num) > -1) {
         
	        this.ticket.crossNumber(num);
	        
	        return true;
	        
        }
        
        return false;

    }

    public setUserOnce(user: User) {

        if(this._user === null)

            this._user = user;

    }

    public toObject() {

        return {
            id: this._id,
            ai: this._ai,
            ticket: this._ticket.toObject(),
            user: this._user.toObject()
        }

    }

}
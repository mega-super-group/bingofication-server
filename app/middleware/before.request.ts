import {App, Request, Response} from "../framework/app";

export class BeforeRequest {

    public static handle(req: Request, res: Response, next: Function) : void {

        App.debug("HTTP::"+req.method)("Request start: " + req.url + " route");
        next();

    }

}
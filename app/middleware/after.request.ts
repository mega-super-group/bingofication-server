import {App, Request, Response} from "../framework/app";

export class AfterRequest {

    public static handle(req: Request, res: Response, next: Function) : void {

        App.debug("HTTP::"+req.method)("Request end: " + req.url + " route");

    }

}
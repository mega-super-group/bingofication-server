import {App, Request, Response} from "../framework/app";

export class ParseRequest {

    public static handle(req: Request, res: Response, next: any) {

        let body: string = "";

        // Make Request JSON field defaulted to null
        req.json = null;

        // Receive data
        req.on("data", part => body += part);

        // Define request body and expected JSON input if any came
        req.on("end", () => {

            req.body = body;

            try {

                req.json = JSON.parse(body);

            } catch (e) {

                App.log("Data came with Request is not JSON compatible","ROUTER::PARSER");

            }

            next();

        })

    }

}
import {App, Request, RequestMethod, Response} from "../app";
import {RouteAccumulator} from "./route.accumulator";
import {NotImplementedException} from "ts-exceptions";

export class RouteParser {

    constructor() {

    }

    /**
     * Prepare routes for HTTP server
     * ------------------------------------------------------------------------------------
     * @param {RouteAccumulator} routeCollection
     */
    public prepareRoutes(routeCollection: RouteAccumulator) {

        const router = App.express;

        /*
         *  Attach requests which should be executed before anything
         *  --------------------------------------------------------
         */
        const beforeRoutes = routeCollection.__getBeforeRoutes();

        beforeRoutes.map(route => router.use(route as any));

        /*
         *  Attach requests which should be executed after anything
         *  --------------------------------------------------------
         */
        const afterRoutes = routeCollection.__getAfterRoutes();

        router.use((req: Request, res: Response, next: any) => {

            res.on("finish", () => {

                afterRoutes.map(route => {

                    route(req, res, ()=>{});

                });

            });

            next();

        });

        /*
         *  Attach list of common routes
         *  --------------------------------------------------------
         */
        const routes = routeCollection.__getRoutes();

        routes.map(route => {

            switch (route.type) {

                case RequestMethod.GET:

                    // Route have list of Middleware
                    if(typeof route.middleware !== "undefined" && route.middleware.length > 0) {

                        router.get(route.path, route.middleware as any, route.to as any);
                        break;

                    }

                    // Route clean
                    router.get(route.path, route.to as any);
                    break;

                case RequestMethod.POST:

                    // Route have list of Middleware
                    if(typeof route.middleware !== "undefined" && route.middleware.length > 0) {

                        router.post(route.path, route.middleware as any, route.to as any);
                        break;

                    }

                    // Route clean
                    router.post(route.path, route.to as any);
                    break;

                case RequestMethod.PUT:

                    // Route have list of Middleware
                    if(typeof route.middleware !== "undefined" && route.middleware.length > 0) {

                        router.put(route.path, route.middleware as any, route.to as any);
                        break;

                    }

                    // Route clean
                    router.put(route.path, route.to as any);
                    break;

                case RequestMethod.DELETE:

                    // Route have list of Middleware
                    if(typeof route.middleware !== "undefined" && route.middleware.length > 0) {

                        router.delete(route.path, route.middleware as any, route.to as any);
                        break;

                    }

                    // Route clean
                    router.delete(route.path, route.to as any);
                    break;

                case RequestMethod.PATCH:

                    // Route have list of Middleware
                    if(typeof route.middleware !== "undefined" && route.middleware.length > 0) {

                        router.patch(route.path, route.middleware as any, route.to as any);
                        break;

                    }

                    // Route clean
                    router.patch(route.path, route.to as any);
                    break;

                default:

                    // Route have not defined method properly
                    throw new NotImplementedException(
                        "RequestMethod: " + route.type + " is not implemented to be routed. "+
                        +"Please edit your routes configuration");

            }

        });

        /*
         *  Error Handling specified
         *  ----------------------------
         */
        const errorHandlers = routeCollection.__getErrorHandlers();

        if(errorHandlers.length > 0) {

            errorHandlers.map(handler => {

                router.use(handler.to as any);

            });

        }
        /*
         *  Error Handling default if no
         *  other handlers defined
         *  ----------------------------
         */
        else {

            const err = (err: any, req:Request, res: Response, next: any) => {

                console.error("Error happened: ", err);
                res.status(500).send("Bad");

            };

            router.use(err as any);

        }

        let notFoundHandlers = routeCollection.__getNotFoundRoutes();
        /*
         *  Not found specified handling
         *  ----------------------------
         */
        if(notFoundHandlers.length > 0) {

            notFoundHandlers.map(handler => {

                router.use(handler.to as any);

            });

        }
        /*
         *   Not found default Handling
         *   ----------------------------
         */
        else {


            const err404 = (req:Request, res: Response, next:any) => {

                console.error("NOT_FOUND 404" + new Date().toISOString() + " : ", req.method, req.url.toString(), req.body);
                res.status(404).send("Not found");

            };

            router.use(err404 as any);

        }

        App.debug("router")("Routes prepared and ready to be served: " + routes.length);

    }

}
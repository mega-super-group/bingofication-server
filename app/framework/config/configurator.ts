export class Configurator {

    private configDataObject: Configuration = null;

    private instanceIsConfigInstance:boolean = false;

    /**
     *
     * -----------------------------------------------------
     * @param {Configuration} configObject
     */
    constructor(configObject: Configuration) {

        this.configDataObject = configObject;

        /*
         *   Set as config object if methods are correct
         */
        if(typeof this.configDataObject.has === "function")
            this.instanceIsConfigInstance = true;


    }

    /**
     * Get config property
     * -----------------------------------------------------
     * @param {string} property
     * @returns {null | string | number}
     */
    public get(property: string) : null & {[key:string]:any} & string & number {

        if(this.instanceIsConfigInstance && this.configDataObject.has(property))

            return this.configDataObject.get(property);

        else

            return null;

    }

}

export interface Configuration {
    get?(property:string) : any;
    has?(property:string) : any;
}

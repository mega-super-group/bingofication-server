import * as http from "http";
import * as path from "path";
import * as express from "express";
import * as debug from "debug";
import * as config from "config";
import * as SIO from "socket.io"

import {Application} from "express";
import {RouteParser} from "./routing/route.parser";
import {RouteAccumulator} from "./routing/route.accumulator";
import {Configurator} from "./config/configurator";
import {IDebug, IDebugger} from "debug";
import {Request as ExpressRequest} from "express"
import {Response as ExpressResponse} from "express";
import {Exception} from "ts-exceptions";
import {Server as WSServer} from "socket.io";

/** -----------------------------------------------------------------
 *                  Main Application Module
 ----------------------------------------------------------------- */
export class App {

    private static _defaultPort = 8989;

    private static _debugInstances: Map<string, IDebugger> = new Map();

    private static _debugEnabled = false;

    private static _initialized = false;

    private static _server: any = null;

    private static _systemPath: any = null;

    private static _express: any = null;

    private static _wsServer: any = null;

    private static _debug: IDebug = null;

    private static _http: any = null;

    private static _router: RouteAccumulator = null;

    private static _routeParser: RouteParser = null;

    private static _configuration: Configurator = null;

    /**
     * Initialize Application services
     * -----------------------------------------------------------------
     */
    public static async init(routesFilePath: string) : Promise<App> {

        console.time("app startup time was:");

        let routeStack: () => void = null;

        try {

            routeStack = require(process.cwd() + '/' + routesFilePath);

        } catch (e) {

            console.error("Route stack cannot be resolved for path: " + routesFilePath +
                " please use correct file name for application routes can be initialized."+
                " Original error: " + e.message + "\n");

        }

        // Apply configuration from config file
        this._configuration = new Configurator(config as any);

        // Create Debug console output environment if application in verbose mode
        if (typeof process.env.DEBUG_OUTPUT !== "undefined")

            this._debugEnabled = true;

        // Initialize Foundation libraries
        this.initFoundation();

        // Initialize List of Application Routes
        this.initRoutes(routeStack);

        // Set as initialized after all configuration got through
        this._initialized = true;

        console.timeEnd("app startup time was:");

        // Return App static
        return this;

    }

    /**
     * Get debug instance for some name
     * -----------------------------------------------------------------
     * @param {string} name
     * @returns {debug.IDebugger}
     */
    public static debug(name = "app:log") : IDebugger {

        const instance = this._debug(name);
        instance.enabled = this._debugEnabled;

        return instance;

    }

    /**
     * Apply Debug log output for some namespace
     * -----------------------------------------------------------------
     * @param {string} message
     * @param {string} namespace
     */
    public static log(message: string | any, namespace:string = "app:log") : void {

        let instance = null;

        if(!this._debugInstances.has(namespace)) {

            instance = this.debug(namespace);
            this._debugInstances.set(namespace, instance);

        } else

            instance = this._debugInstances.get(namespace);

        instance(message);

    }

    /**
     * Apply success log output for some namespace
     * -----------------------------------------------------------------
     * @param {string} message
     * @param {string} namespace
     */
    public static success(message:string | any, namespace:string = "app:success") : void {

        let instance = null;

        if(namespace !== "app:error") {
            namespace += ":error";
        }

        if(!this._debugInstances.has(namespace)) {

            instance = this.debug(namespace);
            this._debugInstances.set(namespace, instance);

        } else

            instance = this._debugInstances.get(namespace);

        message = "\x1b[32m" + "[SUCCESS]" + message;

        instance(message);

    }

    /**
     * Apply Debug error output for some namespace
     * -----------------------------------------------------------------
     * @param {string} message
     * @param {string} namespace
     */
    public static error(message:string | any, namespace:string = "app:error") : void {

        let instance = null;

        if(namespace !== "app:error") {
            namespace += ":error";
        }

        if(!this._debugInstances.has(namespace)) {

            instance = this.debug(namespace);
            this._debugInstances.set(namespace, instance);

        } else

            instance = this._debugInstances.get(namespace);

        message = "\x1b[31m" + "[ERROR]" + message;

        instance(message);

    }

    /**
     * Outputs Express Application instance
     * -----------------------------------------------------------------
     * @returns {any}
     */
    static get express() : Application | null {

        return this._express;

    }

    /**
     * Outputs Application Route Accumulator
     * -----------------------------------------------------------------
     * @returns {RouteAccumulator}
     */
    static get router() : RouteAccumulator {

        return this._router;

    }

    /**
     * Outputs application configuration from attached config file
     * -----------------------------------------------------------------
     * @returns {Configurator}
     */
    static get config() : Configurator {

        return this._configuration;

    }

    /**
     * Outputs webSocket handler
     * -----------------------------------------------------------------
     */
    static get webSocketHandler() : WSServer {

        return this._wsServer;

    }

    /**
     * Start Application to serve web-requests
     * -----------------------------------------------------------------
     */
    public static start() {

        const port = App.config.get("httpPort") !== null ? Number(App.config.get("httpPort")) : this._defaultPort;

        // If Application wasn't initialized prior to be started as server Error should be thrown
        if(!this._initialized)

            throw new Exception("\nApplication not been initialized, can't load dependencies at this time\n", 500);

        // Cache routes before server starts
        this._routeParser.prepareRoutes(this._router);

        this.log("Program start, service loading at port: " + port);

        // Start server according to configuration
        this._server.listen(port);

    }

    /**
     * Initialize Foundation libraries for Bundle platform
     * -----------------------------------------------------------------
     */
    private static initFoundation() {

        process.on('unhandledRejection', (reason) => {
            console.warn("Unhandled rejection registered");
            console.log(reason);
        });

        // Initialize express server
        this._express = express();

        // Apply debug module to App
        this._debug = debug;

        // Apply HTTP module to App
        this._http = http;

        // Apply Path module to App
        this._systemPath = path;

        // Create new Server instance with express handler
        this._server = this._http.Server(this._express);

        this._wsServer = SIO(this._server);

    }

    /**
     * Initialize Routing system
     * -----------------------------------------------------------------
     */
    private static initRoutes(routeStack : any) {

        if(routeStack === null) {
            App.error("Routes cannot be loaded, proper route path should be defined for App.init");
            return;
        }
        
        // If passed variable is object then we should extract first key of object
        if(typeof routeStack === "object")

            routeStack = routeStack[Object.keys(routeStack)[0]];

        // Routestack should be represented by imported module function
        if(typeof routeStack !== "function")

            throw new ConfigurationException("Route stack passed to route initializer is not a function,"+
                " can't initialize routes", 500);

        // Create Route parser instance to parse defined set of routes
        this._routeParser = new RouteParser();

        // Create new Route accumulator for route collecting
        this._router = new RouteAccumulator();

        // Open route file to catch suitable routes and pass them to routing system
        routeStack();

    }

}

/*  -----------------------------------------------------------------
 *
 *
 *               Interfaces, Enumerators and Exceptions
 *               ---------------------------------------
 *
 *     Application using rich set of different interfaces and enum
 *     properties, for fast access and clarity of required interfaces
 *     those will be provided in main module
 *
 *  -----------------------------------------------------------------
 */

/**
 * Configuration Exception
 * -----------------------------------------------------------------
 * Usually thrown when configuration is defined improperly
 */
export class ConfigurationException extends Exception {

}

/**
 * Request interface
 * -----------------------------------------------------------------
 * describing set of parameters which Application method
 * attached to route will be getting when router will call
 * such method after network request
 *
 * (as of current router is Express - IF providing extension)
 */
export interface Request extends ExpressRequest {
    json?: {[key:string]:any};
    auth?: Auth<any>
}

/**
 * Authenticated Request interface
 * -----------------------------------------------------------------
 */
export interface AuthenticatedRequest<T> extends Request {
    auth: Auth<T>
}

/**
 * Response interface
 * -----------------------------------------------------------------
 * describing set of parameters which Application
 * should pass back to network channel as HTTP request
 * answer
 *
 * (as of current router is Express - IF providing extension)
 */
export interface Response extends ExpressResponse
{

}

/**
 * Auth interface for Request objects
 * -----------------------------------------------------------------
 */
export interface Auth<T> {
    type: AuthType;
    data: T;
    authorized: boolean;
}

/**
 * Simple Authentication credentials
 * -----------------------------------------------------------------
 */
export interface AuthCommonCredentials {
    login: string;
    password: string;
}

/**
 * Client Authentication credentials
 * -----------------------------------------------------------------
 */
export interface OauthClientCredentials {
    client: string;
    secret: string;
    application: string;
    scope: string;
}

/**
 * Token bearer credentials
 * -----------------------------------------------------------------
 */
export interface OauthBearerCredentials {
    token: string;
    application: string;
    user: string;
    scope: string;
    expires: number;
}

/**
 * Simple Key bearer credentials
 * -----------------------------------------------------------------
 */
export interface AuthKeyBearerCredentials {
    key: string;
    expiresAt: string;
}

/**
 * Possible types of Authentication for service
 * -----------------------------------------------------------------
 */
export enum AuthType {
    Basic,
    Bearer,
    Session
}

export type RouteHandler = (req: Request, res: Response, next?: RouteHandler) => any;

export type RouteMiddleware = (req: Request, res: Response, next: Function) => void;

export type ErrorRouteHandler = (err: any, req: Request, res: Response, next?: RouteHandler) => Response;

export type ErrorRouteMiddleware = (err: any, req: Request, res: Response, next: Function) => void;

export interface Route {
    type: RequestMethod;
    path: string;
    to: RouteHandler;
    middleware?: RouteMiddleware[];
}

export interface ErrorRoute {
    to: ErrorRouteHandler | RouteHandler;
    middleware?: ErrorRouteMiddleware[];
}

export enum RequestMethod {
    GET,
    POST,
    PUT,
    DELETE,
    PATCH
}
import {Game} from "../../app/models/game/game";
import {GameEngine} from "../../app/models/game/game.engine";
import {expect} from "chai";
import * as Sinon from "sinon";
import {User} from "../../app/models/user/user";
import {UserCredentialsInterface, UserInterface} from "../../app/intefaces/user.inteface";
import {ConnectedUser} from "../../app/models/user/connected.user";
import {SinonExpectation, SinonMock, SinonStub} from "sinon";
import {App} from "../../app/framework/app";

describe("Game engine test", () => {
	
	let game: Game = null;
	let engine: GameEngine = null;
	let user: User = null;
	let userSocketMock: SinonMock = null;
	let userSocketStub: SinonStub = null;
	let userSocketExpectation: SinonExpectation = null;
	let messageQueue: {evt: string, payload: any}[] = [];
	
	/* -----------------------------------------------------------------
	 *                       Prepare environment
	 * -----------------------------------------------------------------*/
	
	before(() => {
		
		return new Promise((resolve => {
			App.init("").then(() => resolve());
		}));
		
	});
	
	/* -----------------------------------------------------------------
	 *                          Prepare tests
	 * -----------------------------------------------------------------*/
	
	beforeEach(() => {
		
		/*
		 *  Create a game
		 */
		game = new Game(1, {
			roundTime: 1,
			inRoundNotificationTime: 100
		});
		
		const anyGame = game as any;
		
		engine = anyGame._engine as GameEngine;
		
		/*
		 *  Mock user/players
		 */
		
		// Create mocking object
		const socketMock = {
			emit: () => {}
		};
		
		// Sinonize it
		userSocketStub = Sinon.stub(socketMock, "emit").callsFake((...args) => {
			messageQueue.push({evt: args[0], payload: args[1]});
		});
		
		// Apply to connection interface
		const userConnection: ConnectedUser = new ConnectedUser();
		userConnection.uid = "321";
		userConnection.socket = socketMock as any;
		
		// Assign secret SID
		const userConfigAny: any = {sid: "123"};
		
		// Assign rest values
		const userConfig: UserInterface & UserCredentialsInterface = userConfigAny;
		userConfig.uid = "321";
		userConfig.name = "Test";
		
		// Create User
		user = new User(userConfig);
		user.connection = userConnection;
		
		// Add User to game
		game.addPlayer(user);
		
	});
	
	afterEach(() => {
		
		game.finishGame();
		
		game = null;
		engine = null;
		user = null;
		userSocketMock = null;
		userSocketExpectation = null;
		messageQueue = [];
		
	});
	
	/* -----------------------------------------------------------------
	 *                              Run tests
	 * -----------------------------------------------------------------*/
	
	it("Should instantiate game engine", () => {
		
		expect(engine).to.not.equal(null, "Game engine should be instantiated");
		
	});
	
	it("Should have and notify players at game start", () => {
		
		game.startGame();
		
		expect(game.players.length).to.equal(1, "Exactly one player should be presented in game");
		expect(messageQueue.length).to.greaterThan(0, "At least one message should be pushed to user");
	
	});
	
	it("Should iterate through the rounds notifying users with proper messages", async () => {
		
		const player = game.getPlayerForUser(user);
		
		expect(player).to.not.equal(null, "Player should be defined for current user");
		
		engine.setPlayerAsReady(player);
		
		game.startGame();
		
		expect(engine.running).to.equal(true, "After game start — GameEngine should be in running state");
		
		/*
			Test game chain to produce set of messages in proper order
		 */
		return new Promise(resolve => {
			
			setTimeout(() => {
				
				expect(messageQueue.length).to.greaterThan(3, "Player should receive at least 4 messages" +
					" after game started and entered active phase");
				
				// Reduce messages to UID identifiers
				const types = messageQueue.reduce((a: string[], b) => {
					
					if(a.indexOf(b.evt) === -1)
						
						a.push(b.evt);
					
					return a;
					
				}, []);
				
				// Test message chain
				expect(types.indexOf("gjn")).to.greaterThan(-1,"Expect player recieve to join message");
				expect(types.indexOf("ginr")).to.greaterThan(0,"Expect one in round update prior game to start");
				expect(types.indexOf("gst")).to.greaterThan(1,"Expect Game Start message to be presented on game start and after game players joined");
				expect(types.indexOf("gnr")).to.greaterThan(2,"Expect new round come up and to be after Game Start message");
				expect(types.indexOf("gmin")).to.greaterThan(3,"Expect Meta information come as the latest");
				
				resolve();
				
			}, 1500);
			
		});
		
	})
	
});
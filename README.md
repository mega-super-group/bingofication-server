# Server for bingo applications

## How to launch

```
npm run dev
```

command above will execute launch script for
your application

## Structure of execution

0) tsconfig.json - compilation
1) `/app/main.ts` - starting points
2) `/app/routes.ts` - routing for next controller

## How to

Please check `src/main.ts` for Application port
parameter and to follow the next steps related to
working with messaging system.

Program starts with the main.ts file:

```typescript
App.init("dist/routes").then(() => {

    /*
     *  Here is logic which should be initialized before app start
     */

    GameInitializer.init();

    /*
     *  App start
     */
    App.start();

});

```

Where the game initializer is the thing which
maintains the execution of GameInitializer model .

## Local docker

Please run:
```
sh build-local.sh
```
for build docker image

```
sh deploy-local.sh
```
for deploy this image to docker daemon
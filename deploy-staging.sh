#!/usr/bin/env bash
eval $(docker-machine env do-docker-1)
docker container stop bingo-server
docker container rm bingo-server
docker run -p 8989:8989 -d --name bingo-server local/bingo-server:latest
